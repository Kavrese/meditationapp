package com.ege.mediationapp.AuthActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ege.mediationapp.MainActivity.MainActivity
import com.ege.mediationapp.R
import com.ege.mediationapp.RegActivity.RegActivity
import com.ege.mediationapp.common.*
import com.ege.mediationapp.models.ModelAnswerLogin
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.activity_on_boarding.*

object UserData{
    var modelAnswerLogin: ModelAnswerLogin? = null
}

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        auth.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()
            if (!validateEmail(email)) {
                showAlertDialog(this, "Ошибка авторизации", "Не корректная почта")
                return@setOnClickListener
            }
            if (email.isEmpty() || password.isEmpty()) {
                showAlertDialog(this, "Ошибка авторизации", "Заполните все поля")
                return@setOnClickListener
            }

            auth(this, email, password, object: OnCompleteAuth {
                override fun onComplete(answerLogin: ModelAnswerLogin) {
                    UserData.modelAnswerLogin = answerLogin
                    getSharedPreferences("0", 0).edit()
                        .putBoolean("isFirst", false)
                        .apply()
                    startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                    finish()
                }
            })
        }

        to_reg_2.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }
    }
}