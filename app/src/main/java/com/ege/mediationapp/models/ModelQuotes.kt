package com.ege.mediationapp.models

data class ModelQuotes(
    val success: Boolean,
    val data: List<DataQuotes>
)

data class DataQuotes(
    val id: Int,
    val title: String,
    val description: String,
    val image: String
)
