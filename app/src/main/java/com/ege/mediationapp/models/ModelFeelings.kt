package com.ege.mediationapp.models

data class ModelFeelings(
    val success: Boolean,
    val data: List<DataFeelings>
)

data class DataFeelings(
    val id: Int,
    val title: String,
    val position: Int,
    val image: String
)
