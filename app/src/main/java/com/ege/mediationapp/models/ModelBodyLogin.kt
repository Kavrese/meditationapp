package com.ege.mediationapp.models

data class ModelBodyLogin(
    val email: String,
    val password: String
)
