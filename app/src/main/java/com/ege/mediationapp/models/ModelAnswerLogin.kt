package com.ege.mediationapp.models

data class ModelAnswerLogin(
    val id: String,
    val email: String,
    val avatar: String,
    val token: String,
    val nickName: String
)
