package com.ege.mediationapp.OnBoardingActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ege.mediationapp.AuthActivity.AuthActivity
import com.ege.mediationapp.R
import com.ege.mediationapp.RegActivity.RegActivity
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        to_auth.setOnClickListener{
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }
        to_reg.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }
    }
}