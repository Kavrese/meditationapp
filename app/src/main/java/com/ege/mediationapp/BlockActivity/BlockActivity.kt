package com.ege.mediationapp.BlockActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.ege.mediationapp.OnBoardingActivity.OnBoardingActivity
import com.ege.mediationapp.R
import com.ege.mediationapp.common.initRetrofit
import com.ege.mediationapp.common.showAlertDialog
import com.ege.mediationapp.models.ModelFeelings
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_block.*
import kotlinx.android.synthetic.main.custom_tab.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BlockActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_block)

        pager2.adapter = AdapterBlock()

        val TAG = "Ошибка получения списка чувств"
        initRetrofit().feelings().enqueue(object: Callback<ModelFeelings>{
            override fun onResponse(call: Call<ModelFeelings>, response: Response<ModelFeelings>) {
                if (response.isSuccessful && response.body() != null && response.body()!!.success){
                    (pager2.adapter as AdapterBlock).updateData(response.body()!!.data)
                    initPagerTab()
                }else{
                    showAlertDialog(applicationContext, TAG)
                }
            }

            override fun onFailure(call: Call<ModelFeelings>, t: Throwable) {
                showAlertDialog(applicationContext, TAG, t)
            }

        })
    }

    private fun initPagerTab(){
        tab.setOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot.setImageDrawable(ContextCompat.getDrawable(this@BlockActivity, R.drawable.dot_activ))
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot.setImageDrawable(ContextCompat.getDrawable(this@BlockActivity, R.drawable.dot))
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
        TabLayoutMediator(tab, pager2){ tabItem, p0 ->
            tabItem.customView = LayoutInflater.from(this).inflate(R.layout.custom_tab, tabItem.view, false)
        }.attach()
        pager2.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tab.selectTab(tab.getTabAt(position))
                if (position == 0){
                    left.text = "Пропустить"
                    left.setOnClickListener {
                        startActivity(Intent(this@BlockActivity, OnBoardingActivity::class.java))
                        finish()
                    }
                    return
                }
                if (position == pager2.adapter!!.itemCount - 1){
                    right.text = "Аутентификация"
                    right.setOnClickListener {
                        startActivity(Intent(this@BlockActivity, OnBoardingActivity::class.java))
                        finish()
                    }
                    return
                }
                right.text = "Далее"
                right.setOnClickListener {
                    pager2.setCurrentItem(position + 1, true)
                }
                left.text = "Назад"
                left.setOnClickListener {
                    pager2.setCurrentItem(position - 1, true)
                }
            }
        })
    }
}