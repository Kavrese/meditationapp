package com.ege.mediationapp.BlockActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ege.mediationapp.R
import com.ege.mediationapp.models.DataFeelings
import com.ege.mediationapp.models.ModelFeelings
import kotlinx.android.synthetic.main.item_block.view.*

class SimpleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){}

class AdapterBlock(private val listBlocks: MutableList<DataFeelings> = mutableListOf()): RecyclerView.Adapter<SimpleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_block, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.title_block.text = listBlocks[position].title
        Glide.with(holder.itemView)
            .load(listBlocks[position].image)
            .into(holder.itemView.cover_block)
    }

    override fun getItemCount(): Int = listBlocks.size

    fun updateData(newListBlocks: List<DataFeelings>){
        listBlocks.clear()
        listBlocks.addAll(newListBlocks)
        notifyDataSetChanged()
    }
}