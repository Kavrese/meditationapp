package com.ege.mediationapp.MainActivity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ege.mediationapp.MainActivity.Fragments.FragmentHome
import com.ege.mediationapp.MainActivity.Fragments.FragmentProfile
import com.ege.mediationapp.MainActivity.Fragments.FragmentSound

class FragmentsAdapter(fa: FragmentActivity): FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return arrayListOf(FragmentHome(), FragmentSound(), FragmentProfile())[position]
    }
}