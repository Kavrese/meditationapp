package com.ege.mediationapp.MainActivity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ege.mediationapp.BlockActivity.SimpleViewHolder
import com.ege.mediationapp.R
import com.ege.mediationapp.models.DataFeelings
import com.ege.mediationapp.models.DataQuotes
import kotlinx.android.synthetic.main.item_feeling.view.*
import kotlinx.android.synthetic.main.item_quotes.view.*

class AdapterQuotes(private val listQuotes: MutableList<DataQuotes> = mutableListOf()): RecyclerView.Adapter<SimpleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_quotes, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.title_quotes.text = listQuotes[position].title
    }

    override fun getItemCount(): Int = listQuotes.size

    fun updateData(newListBlocks: List<DataQuotes>){
        listQuotes.clear()
        listQuotes.addAll(newListBlocks)
        notifyDataSetChanged()
    }

}