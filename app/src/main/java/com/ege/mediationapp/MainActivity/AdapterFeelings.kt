package com.ege.mediationapp.MainActivity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ege.mediationapp.BlockActivity.SimpleViewHolder
import com.ege.mediationapp.R
import com.ege.mediationapp.models.DataFeelings
import kotlinx.android.synthetic.main.item_feeling.view.*

class AdapterFeelings(private val listBlocks: MutableList<DataFeelings> = mutableListOf()): RecyclerView.Adapter<SimpleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_feeling, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        Glide.with(holder.itemView)
            .load(listBlocks[position].image)
            .into(holder.itemView.cover_feeling)
        holder.itemView.title_feeling.text = listBlocks[position].title
    }

    override fun getItemCount(): Int = listBlocks.size

    fun updateData(newListBlocks: List<DataFeelings>){
        listBlocks.clear()
        listBlocks.addAll(newListBlocks)
        notifyDataSetChanged()
    }

}