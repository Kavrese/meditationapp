package com.ege.mediationapp.MainActivity.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.ege.mediationapp.AuthActivity.UserData
import com.ege.mediationapp.BlockActivity.AdapterBlock
import com.ege.mediationapp.MainActivity.AdapterFeelings
import com.ege.mediationapp.MainActivity.AdapterQuotes
import com.ege.mediationapp.R
import com.ege.mediationapp.common.initRetrofit
import com.ege.mediationapp.common.showAlertDialog
import com.ege.mediationapp.models.ModelFeelings
import com.ege.mediationapp.models.ModelQuotes
import kotlinx.android.synthetic.main.activity_block.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentHome: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        welcome_text.text = welcome_text.text.toString() + UserData.modelAnswerLogin!!.nickName + "!"
        Glide.with(this)
            .load(UserData.modelAnswerLogin!!.avatar)
            .into(avatar_user)
        rec_feelings.apply {
            adapter = AdapterFeelings()
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
        rec_quotes.apply{
            adapter = AdapterQuotes()
            layoutManager = LinearLayoutManager(requireContext())
        }

        initRetrofit().feelings().enqueue(object: Callback<ModelFeelings> {
            override fun onResponse(call: Call<ModelFeelings>, response: Response<ModelFeelings>) {
                if (response.isSuccessful && response.body() != null && response.body()!!.success){
                    (rec_feelings.adapter as AdapterFeelings).updateData(response.body()!!.data)
                }else{
                    showAlertDialog(requireContext(), "Ошибка получения списка чувств")
                }
            }

            override fun onFailure(call: Call<ModelFeelings>, t: Throwable) {
                showAlertDialog(requireContext(), "Ошибка получения списка чувств", t)
            }

        })

        initRetrofit().quotes().enqueue(object: Callback<ModelQuotes>{
            override fun onResponse(call: Call<ModelQuotes>, response: Response<ModelQuotes>) {
                if (response.isSuccessful && response.body() != null && response.body()!!.success){
                    (rec_quotes.adapter as AdapterQuotes).updateData(response.body()!!.data)
                }else{
                    showAlertDialog(requireContext(), "Ошибка получения списка чувств")
                }
            }

            override fun onFailure(call: Call<ModelQuotes>, t: Throwable) {
                showAlertDialog(requireContext(), "Ошибка получения списка цитат", t)
            }

        })
    }
}
