package com.ege.mediationapp.MainActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ege.mediationapp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pager2_main.adapter = FragmentsAdapter(this)
        pager2_main.isUserInputEnabled = false
        bottom_nav_view.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home -> pager2_main.setCurrentItem(0, false)
                R.id.sound -> pager2_main.setCurrentItem(1, false)
                R.id.profile -> pager2_main.setCurrentItem(2, false)
            }
            return@setOnItemSelectedListener true
        }
    }
}