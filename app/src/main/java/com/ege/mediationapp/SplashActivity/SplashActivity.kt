package com.ege.mediationapp.SplashActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.ege.mediationapp.BlockActivity.BlockActivity
import com.ege.mediationapp.OnBoardingActivity.OnBoardingActivity
import com.ege.mediationapp.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler(Looper.getMainLooper()).postDelayed({
            val intent = if (getSharedPreferences("0", 0).getBoolean("isFirst", true)){
                Intent(this, BlockActivity::class.java)
            }else{
                Intent(this, OnBoardingActivity::class.java)
            }
            startActivity(intent)
            finish()
        }, 1500)
    }
}