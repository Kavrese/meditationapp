package com.ege.mediationapp.common

import com.ege.mediationapp.models.ModelAnswerLogin

interface OnCompleteAuth {
    fun onComplete(answerLogin: ModelAnswerLogin)
}