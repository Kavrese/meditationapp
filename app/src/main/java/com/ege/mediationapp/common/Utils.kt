package com.ege.mediationapp.common

import android.app.AlertDialog
import android.content.Context
import com.ege.mediationapp.models.ModelAnswerLogin
import com.ege.mediationapp.models.ModelBodyLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun initRetrofit(): API{
    return Retrofit.Builder()
        .baseUrl("http://mskko2021.mad.hakta.pro/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(API::class.java)
}

fun showAlertDialog(context: Context, title: String, message: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("ok", null)
        .show()
}

fun showAlertDialog(context: Context, title: String, t: Throwable){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(t.message)
        .setNegativeButton("ok", null)
        .show()
}

fun showAlertDialog(context: Context, title: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage("Повторите попытку позже")
        .setNegativeButton("ok", null)
        .show()
}

fun validateEmail(email: String): Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun auth(context: Context, email: String, password: String, onCompleteAuth: OnCompleteAuth){
    val TAG = "Ошибка авторизации"
    initRetrofit().login(ModelBodyLogin(email, password)).enqueue(object: Callback<ModelAnswerLogin>{
        override fun onResponse(
            call: Call<ModelAnswerLogin>,
            response: Response<ModelAnswerLogin>
        ) {
            if (response.isSuccessful && response.body() != null){
                onCompleteAuth.onComplete(response.body()!!)
            }else{
                showAlertDialog(context, TAG)
            }
        }

        override fun onFailure(call: Call<ModelAnswerLogin>, t: Throwable) {
            showAlertDialog(context, TAG, t)
        }

    })
}