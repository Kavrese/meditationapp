package com.ege.mediationapp.common

import com.ege.mediationapp.models.ModelAnswerLogin
import com.ege.mediationapp.models.ModelBodyLogin
import com.ege.mediationapp.models.ModelFeelings
import com.ege.mediationapp.models.ModelQuotes
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface API {
    @POST("user/login")
    fun login (@Body body: ModelBodyLogin): Call<ModelAnswerLogin>

    @GET("quotes")
    fun quotes(): Call<ModelQuotes>

    @GET("feelings")
    fun feelings(): Call<ModelFeelings>
}